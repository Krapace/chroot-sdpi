################
# CHROOT TO PI #
################

This script allows you to chroot ("work on") 
the raspbian sd card as if it's the raspberry pi
on your Ubuntu desktop/laptop
just much faster and more convenient

credits: https://gist.github.com/jkullick/9b02c2061fbdf4a6c4e8a78f1312a689

make sure you have issued
(sudo) apt install qemu qemu-user-static binfmt-support

Write the raspbian image onto the sd card,
boot the pi with the card once 
so it expands the fs automatically
then plug back to your laptop/desktop
and chroot to it with this script.

Invoke:
(sudo) ./chroot-to-pi.sh /dev/sdb 
assuming /dev/sdb is your sd-card
commands to help you: 
lsblk
or 
dmesg |tail -n 30

Note: If you have an image file instead of the sd card, 
you will need to issue 
(sudo) apt install kpartx

03.01.2020
- add feature access internet
- add function usage
